#include <iostream>
#include <vector>

int main() {
    unsigned int height;
    std::cin >> height;

    unsigned int width;
    std::cin >> width;

    unsigned int num_of_bricks;
    std::cin >> num_of_bricks;

    unsigned int current_width = 0;
    unsigned int current_height = 0;

    for (int i = 0; i < num_of_bricks; i++) {
        int brick_size;
        std::cin >> brick_size;

        current_width += brick_size;

        if (current_width > width) {
            std::cout << "NO" << std::endl;
            return 0;
        }

        if (current_width == width) {
            current_height++;
            current_width = 0;
        }
    }

    std::cout << "YES" << std::endl;

    return 0;
}

