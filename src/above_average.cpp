#include <iostream>
#include <iomanip>
#include <vector>

int main() {
    unsigned int tests;
    std::cin >> tests;

    for (int i = 0; i < tests; i++) {
        unsigned int sample_size;
        std::cin >> sample_size;

        std::vector<unsigned int> tests(sample_size);

        unsigned int sum = 0;
        for (int i = 0; i < sample_size; i++) {
            unsigned int test_result;
            std::cin >> test_result;
            tests[i] = test_result;

            sum += test_result;
        }

        double avg = static_cast<double>(sum) / static_cast<double>(sample_size);

        int count = 0;
        for (auto& result : tests) {
            if (static_cast<double>(result) > avg) {
                count++;
            }
        }

        std::cout
            << std::fixed
            << std::setprecision(3)
            << (static_cast<double>(count) / static_cast<double>(sample_size)) * 100.0
            << '%'
            << std::endl;
    }
    return 0;
}
