#include <iostream>

int main() {
    // infinite loop
    for (;;) {
        // get number of inputs
        int size;
        std::cin >> size;

        // exit condition
        if (size == -1) {
            break;
        }

        // loop through each input and calculate speed
        int total_speed = 0;
        int total_elapsed_time = 0;
        for (int i = 0; i < size; i++) {
            // store inputs
            int limit;
            int time;
            std::cin >> limit >> time;

            // add inputs to total_speed
            total_speed += limit * (time - total_elapsed_time);

            // add time to elapsed time
            total_elapsed_time = time;
        }

        // print output
        std::cout << total_speed << " miles" << std::endl;
    }
    return 0;
}
