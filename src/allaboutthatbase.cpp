#include <iostream>
#include <string>

enum Operator {
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION,
};

struct expression {
    std::string x_value;
    std::string y_value;
    Operator op;
    std::string result;
};

expression serialize_exp(const std::string& str) {

}

int main() {
    int input_size;
    std::cin >> input_size;

    for (int i = 0; i < input_size; i++) {
        std::string exp;
        std::getline(std::cin, exp);

        std::cout << "Line is: " << exp << std::endl;
    }

    return 0;
}
